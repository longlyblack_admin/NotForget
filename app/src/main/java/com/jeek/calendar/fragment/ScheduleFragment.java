package com.jeek.calendar.fragment;

import android.annotation.SuppressLint;
import android.app.Service;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.jeek.calendar.R;
import com.jeek.calendar.activity.MainActivity;
import com.jeek.calendar.adapter.ScheduleAdapter;
import com.jeek.calendar.dialog.SelectDateDialog;
import com.jeek.calendar.task.schedule.AddScheduleTask;
import com.jeek.calendar.task.schedule.LoadScheduleTask;
import com.jeek.calendar.utils.BHThreadPool;
import com.jeek.calendar.utils.LogUtil;
import com.jeek.calendar.utils.TimeUtils;
import com.jeek.calendar.widget.calendar.OnCalendarClickListener;
import com.jeek.calendar.widget.calendar.schedule.ScheduleLayout;
import com.jeek.calendar.widget.calendar.schedule.ScheduleRecyclerView;
import com.jimmy.common.base.app.BaseFragment;
import com.jimmy.common.bean.Schedule;
import com.jimmy.common.data.ScheduleDao;
import com.jimmy.common.listener.OnTaskFinishedListener;
import com.jimmy.common.util.DeviceUtils;
import com.jimmy.common.util.ToastUtils;
import com.loonggg.lib.alarmmanager.clock.AlarmManagerUtil;
import com.loonggg.lib.alarmmanager.clock.SimpleDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by Jimmy on 2016/10/11 0011.
 */
public class ScheduleFragment extends BaseFragment implements OnCalendarClickListener, View.OnClickListener,
        OnTaskFinishedListener<List<Schedule>>, SelectDateDialog.OnSelectDateListener {

    private ScheduleLayout slSchedule;
    private ScheduleRecyclerView rvScheduleList;
    private EditText etInputContent;
    private RelativeLayout rLNoTask;
    private boolean isRun = true;

    private ScheduleAdapter mScheduleAdapter;
    private int mCurrentSelectYear, mCurrentSelectMonth, mCurrentSelectDay;
    private long mTime;

    private List<Schedule> list = new ArrayList<>();//数据源
    private List<Schedule> listSecond = new ArrayList<>();//数据源

    /**
     * 手机振动器
     */
    private Vibrator vibrator;

    public static ScheduleFragment getInstance() {
        return new ScheduleFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isRun = false;
    }

    @Nullable
    @Override
    protected View initContentView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    protected void bindView() {
        slSchedule = searchViewById(R.id.slSchedule);
        etInputContent = searchViewById(R.id.etInputContent);
        rLNoTask = searchViewById(R.id.rlNoTask);
        slSchedule.setOnCalendarClickListener(this);
        searchViewById(R.id.ibMainClock).setOnClickListener(this);
        searchViewById(R.id.ibMainOk).setOnClickListener(this);
        initScheduleList();
        initBottomInputBar();
    }

    @Override
    protected void initData() {
        super.initData();
        initDate();
    }

    @Override
    protected void bindData() {
        super.bindData();
        resetScheduleList();
    }

    public void resetScheduleList() {
        new LoadScheduleTask(mActivity, this, mCurrentSelectYear, mCurrentSelectMonth, mCurrentSelectDay).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void initDate() {
        // 震动效果的系统服务
        vibrator = (Vibrator) getActivity().getSystemService(VIBRATOR_SERVICE);
        /**
         * 初始化日历和弹窗
         */
        Calendar calendar = Calendar.getInstance();
        setCurrentSelectDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onResume() {
        super.onResume();
        isRun = true;
        /**
         * 开启子线程
         * 每隔六十秒循环一次
         *
         * 获取数据库数据
         * 循环数据判断日期是否需要提醒
         * 进行提醒
         */
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (isRun) {
                    list = ScheduleDao.getInstance(getActivity()).getAll();
                    LogUtil.e("列表长度-->" + list.size());
                    if (list.size() > 0) {
                        for (Schedule schedule : list) {
                            if (System.currentTimeMillis() - schedule.getTime() == 0) {
                                Message msg = Message.obtain();
                                msg.obj = schedule;
                                msg.what = 0;
                                handler.sendMessage(msg);
                            }
                        }
                    }
                }
            }
        };
        BHThreadPool.getInstance().executeTask(runnable);
    }

    @SuppressLint("HandlerLeak")
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Schedule schedule = (Schedule) msg.obj;
            LogUtil.e("开始提醒-->" + schedule.getTitle());
            showDialogInBroadcastReceiver(schedule.getTitle(), 0);
        }
    };

    private void showDialogInBroadcastReceiver(String message, final int flag) {
        //数组参数意义：第一个参数为等待指定时间后开始震动，震动时间为第二个参数。后边的参数依次为等待震动和震动的时间
        //第二个参数为重复次数，-1为不重复，0为一直震动
        if (flag == 0 || flag == 2) {
            vibrator = (Vibrator) getActivity().getSystemService(Service.VIBRATOR_SERVICE);
            vibrator.vibrate(new long[]{100, 10, 100, 600}, 0);
        }

        final SimpleDialog dialog = new SimpleDialog(getActivity(), com.loonggg.lib.alarmmanager.clock.R.style.Theme_dialog);
        dialog.show();
        dialog.setTitle("闹钟提醒");
        dialog.setMessage(message);
        dialog.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClickDate(int year, int month, int day) {
        setCurrentSelectDate(year, month, day);
        resetScheduleList();
    }

    @Override
    public void onPageChange(int year, int month, int day) {

    }

    private void initScheduleList() {
        rvScheduleList = slSchedule.getSchedulerRecyclerView();
        LinearLayoutManager manager = new LinearLayoutManager(mActivity);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvScheduleList.setLayoutManager(manager);
        DefaultItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setSupportsChangeAnimations(false);
        rvScheduleList.setItemAnimator(itemAnimator);
        mScheduleAdapter = new ScheduleAdapter(mActivity, this);
        rvScheduleList.setAdapter(mScheduleAdapter);
    }

    private void initBottomInputBar() {
        etInputContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etInputContent.setGravity(s.length() == 0 ? Gravity.CENTER : Gravity.CENTER_VERTICAL);
            }
        });
        etInputContent.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibMainClock:
                showSelectDateDialog();
                break;
            case R.id.ibMainOk:
                addSchedule();
                break;
        }
    }

    private void showSelectDateDialog() {
        new SelectDateDialog(mActivity, this, mCurrentSelectYear,
                mCurrentSelectMonth, mCurrentSelectDay,
                slSchedule.getMonthCalendar().getCurrentItem()).show();
    }

    private void closeSoftInput() {
        etInputContent.clearFocus();
        DeviceUtils.closeSoftInput(mActivity, etInputContent);
    }

    private void addSchedule() {
        String content = etInputContent.getText().toString();
        if (TextUtils.isEmpty(content)) {
            ToastUtils.showShortToast(mActivity, R.string.schedule_input_content_is_no_null);
        } else {
            closeSoftInput();
            Schedule schedule = new Schedule();
            schedule.setTitle(content);
            schedule.setState(0);
            schedule.setTime(mTime);
            schedule.setYear(mCurrentSelectYear);
            schedule.setMonth(mCurrentSelectMonth);
            schedule.setDay(mCurrentSelectDay);
            new AddScheduleTask(mActivity, new OnTaskFinishedListener<Schedule>() {
                @Override
                public void onTaskFinished(Schedule data) {
                    if (data != null) {
                        mScheduleAdapter.insertItem(data);
                        etInputContent.getText().clear();
                        rLNoTask.setVisibility(View.GONE);
                        mTime = 0;
                        updateTaskHintUi(mScheduleAdapter.getItemCount() - 2);
                    }
                }
            }, schedule).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void setCurrentSelectDate(int year, int month, int day) {
        mCurrentSelectYear = year;
        mCurrentSelectMonth = month;
        LogUtil.e("设置弹窗月份-->" + month);
        mCurrentSelectDay = day;
        if (mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).resetMainTitleDate(year, month, day);
        }
    }

    @Override
    public void onTaskFinished(List<Schedule> data) {
        mScheduleAdapter.changeAllData(data);
        rLNoTask.setVisibility(data.size() == 0 ? View.VISIBLE : View.GONE);
        updateTaskHintUi(data.size());
    }

    private void updateTaskHintUi(int size) {
        if (size == 0) {
            slSchedule.removeTaskHint(mCurrentSelectDay);
        } else {
            slSchedule.addTaskHint(mCurrentSelectDay);
        }
    }

    @Override
    public void onSelectDate(final int year, final int month, final int day, long time, int position) {
        slSchedule.getMonthCalendar().setCurrentItem(position);
        slSchedule.postDelayed(new Runnable() {
            @Override
            public void run() {
                slSchedule.getMonthCalendar().getCurrentMonthView().clickThisMonth(year, month, day);
            }
        }, 100);
        mTime = time;
    }

    public int getCurrentCalendarPosition() {
        return slSchedule.getMonthCalendar().getCurrentItem();
    }

}
