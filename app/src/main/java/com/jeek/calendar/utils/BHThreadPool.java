package com.jeek.calendar.utils;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BHThreadPool {

    ThreadPoolExecutor threadPoolExecutor;
    int corePoolSize;
    int maximumPollSize;
    long keepAliveTime;
    public static BHThreadPool instance;

    public static BHThreadPool getInstance() {
        if (instance == null) {
            instance = new BHThreadPool(5,10,100);
        }
        return instance;
    }

    public BHThreadPool(int corePoolSize, int maximumPollSize, long keepAliveTime){
        this.corePoolSize = corePoolSize;
        this.maximumPollSize = maximumPollSize;
        this.keepAliveTime = keepAliveTime;
    }

    public ThreadPoolExecutor initExecutor(){
        if (threadPoolExecutor == null) {
            synchronized (BHThreadPool.class){
                TimeUnit unit =  TimeUnit.MILLISECONDS;
                ThreadFactory threadFactory = Executors.defaultThreadFactory();
                RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
                LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();

                threadPoolExecutor = new ThreadPoolExecutor(
                        //核心线程数
                        corePoolSize,
                        //最大线程数
                        maximumPollSize,
                        //保持时间
                        keepAliveTime,
                        //保持时间对应的单位
                        unit,
                        workQueue,
                        threadFactory,
                        handler
                );
            }
        }
        return threadPoolExecutor;
    }

    /**
     * 执行任务
     */
    public void executeTask(Runnable runnable){
        initExecutor();
        threadPoolExecutor.execute(runnable);
    }

    /**
     * 提交任务
     */
    public Future<?> commitTask(Runnable runnable){
        initExecutor();
        return threadPoolExecutor.submit(runnable);
    }

    /**
     * 删除任务
     * removeTask（）方法起作用有一个必要的前提，就是这个任务还没有开始执行，
     * 如果已经开始执行了，就停止不了该任务了，这个方法就不会起作用
     */
    public void removeTask(Runnable runnable){
        initExecutor();
        threadPoolExecutor.remove(runnable);
    }

    /**
     * 关闭线程池操作的方法
     */
    public void closeThread(){
        threadPoolExecutor.shutdownNow();
    }

    /**
     * 线程池用法
     * 1 BHThreadPool 线程池对象
     * 2 runnable
     * 3 将runnable添加到BHThreadPool线程池中去
     */

    //    BHThreadPool BHThreadPool = new BHThreadPool(1,1,1000);
    //    Runnable runnable = new Runnable() {
    //        @Override
    //        public void run() {
    //    可执行发动handler操作
    //            Log.e("log", "子线程操作");
    //        }
    //    };
    //
    //    BHThreadPool.executeTask(runnable);

}
