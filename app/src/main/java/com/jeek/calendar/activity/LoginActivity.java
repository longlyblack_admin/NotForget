package com.jeek.calendar.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jeek.calendar.R;
import com.jeek.calendar.utils.PermissionUtil;
import com.jeek.calendar.utils.SPUtil;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.util.ToastUtils;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText etModulePhone, etModulePwd;
    private Button btnModuleLogin, btModuleRegister;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_login);

        String phone = (String) SPUtil.get(LoginActivity.this, "module_is_login", "");
        if (!TextUtils.isEmpty(phone)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        //获取录音、存储权限，以便使用科大讯飞的语音功能
        initPermission();

        etModulePhone = (EditText) findViewById(R.id.etModulePhone);
        etModulePwd = (EditText) findViewById(R.id.etModulePwd);
        btnModuleLogin = (Button) findViewById(R.id.btModuleLogin);
        btModuleRegister = (Button) findViewById(R.id.btModuleRegister);
        btnModuleLogin.setOnClickListener(this);
        btModuleRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int sS_id = v.getId();
        if (sS_id == R.id.btModuleLogin) {
            String phone = etModulePhone.getText().toString().trim();
            if (TextUtils.isEmpty(phone)) {
                ToastUtils.showToast(LoginActivity.this, "请输入手机号码");
                return;
            }

            if (phone.length() != 11) {
                ToastUtils.showToast(LoginActivity.this, "请输入正确的手机号码");
                return;
            }

            String pwd = etModulePwd.getText().toString().trim();
            if (TextUtils.isEmpty(pwd)) {
                ToastUtils.showToast(LoginActivity.this, "请输入密码");
                return;
            }

            String p = (String) SPUtil.get(LoginActivity.this, "module_phone", "");
            if (!p.equals(phone)) {
                ToastUtils.showToast(LoginActivity.this, "账号错误");
                return;
            }

            String d = (String) SPUtil.get(LoginActivity.this, "module_pwd", "");
            if (!d.equals(pwd)) {
                ToastUtils.showToast(LoginActivity.this, "密码错误");
                return;
            }

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else if (sS_id == R.id.btModuleRegister) {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        }
    }

    private void initPermission() {
        PermissionUtil.loginPermission(this, 0x0123);
    }
}
