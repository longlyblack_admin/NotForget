package com.jeek.calendar.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jeek.calendar.R;
import com.jeek.calendar.utils.SPUtil;
import com.jimmy.common.base.app.BaseActivity;
import com.jimmy.common.util.ToastUtils;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private EditText etModulePhone ,etModulePwd;
    private Button btnModuleLogin;

    @Override
    protected void bindView() {
        setContentView(R.layout.activity_register);
        etModulePhone = (EditText) findViewById(R.id.etModulePhone);
        etModulePwd  = (EditText) findViewById(R.id.etModulePwd);
        btnModuleLogin = (Button) findViewById(R.id.btModuleLogin);
        btnModuleLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int sS_id = v.getId();
        if (sS_id == R.id.btModuleLogin) {
            String phone = etModulePhone.getText().toString().trim();
            if (TextUtils.isEmpty(phone)) {
                ToastUtils.showToast(RegisterActivity.this,"请输入手机号码");
                return;
            }

            if (phone.length() != 11) {
                ToastUtils.showToast(RegisterActivity.this,"请输入正确的手机号码");
                return;
            }

            String pwd = etModulePwd.getText().toString().trim();
            if (TextUtils.isEmpty(pwd)) {
                ToastUtils.showToast(RegisterActivity.this,"请输入密码");
                return;
            }

            SPUtil.put(RegisterActivity.this,"module_phone",phone);
            SPUtil.put(RegisterActivity.this,"module_pwd",pwd);

            startActivity(new Intent(RegisterActivity.this,MainActivity.class));
            finish();
        }
    }
}
