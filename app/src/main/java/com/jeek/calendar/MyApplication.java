package com.jeek.calendar;

import android.app.Application;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.jeek.calendar.utils.LogUtil;

public class MyApplication extends Application {

    public static MyApplication instance;

    public static MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        LogUtil.init(true);
        SpeechUtility.createUtility(this, SpeechConstant.APPID +"=5ebd1e4d");
    }
}
